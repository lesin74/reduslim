'use strict';

const selectCountryRow = document.querySelector('.select_country_row');
const selectCountryItems = document.querySelectorAll('.select_country_items');
const selectCountryItem = document.querySelectorAll('.select_country_item');

for (let i = 0; i < selectCountryItems.length; i++) {
    selectCountryRow.addEventListener('click', function (event) {
        selectCountryItems[i].classList.toggle('select_country_items_active');
        selectCountryRow.classList.toggle('select_country_row_active');
    });
    selectCountryItems[i].addEventListener('click', function (event) {
        selectCountryRow.innerHTML = event.target.innerHTML;
        selectCountryRow.classList.toggle('select_country_row_active');
        selectCountryItems[i].classList.toggle('select_country_items_active');
    });
}
